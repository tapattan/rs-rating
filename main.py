from fastapi import FastAPI

from fastapi.responses import HTMLResponse
from fastapi import Body

import pandas as pd
import numpy as np 

from tvDatafeed import TvDatafeed, Interval
 
from helper import getFundamentalInSector
from helper import getCss
from helper import getMemberOfIndex,HistStockPrice,rankWithRange

from helper import getRSRanking,findRangeDateRS


app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

#######################################################################################################################
@app.get("/relative_strength_rating",response_class=HTMLResponse)
def get_Relative_Strength_Rating():
   
   beginDate,endDate = findRangeDateRS()
   #print(  beginDate,endDate)
   RS = getRSRanking(beginDate,endDate)
        
   h = '<div>Relative Strength Rating : SET100</div>'
   h+= '<div>'+beginDate+'-'+endDate+'</div>'
   
   #RS = RS.reset_index()
   RS = RS[['no','symbol','RS','RANK','RS Rank']]
        
   return getCss()+h+RS.to_html(index=False) 


#######################################################################################################################
#SET50,SET100,sSET,SETCLMV,SETHD,SETTHSI,SETWB
@app.post("/memberOfIndex/{index_type}")
def get_Member_Of_Index(indextype: str = Body(...,
        examples={'type1':{'summary':'SET50','value':'SET50'},
                  'type2':{'summary':'SET100','value':'SET100'},
                  'type3':{'summary':'sSET','value':'sSET'},
                  'type4':{'summary':'SETCLMV','value':'SETCLMV'},
                  'type5':{'summary':'SETHD','value':'SETHD'},
                  'type6':{'summary':'SETTHSI','value':'SETTHSI'},
        })):
    print('**',str(indextype))
    df = getMemberOfIndex(indextype)
    if(type(df)==int):
       return 'not support<br>ex. SET50,SET100,sSET,SETCLMV,SETHD,SETTHSI' 
    return df 

#######################################################################################################################
@app.get("/fundamentalInSector/{symbol}", response_class=HTMLResponse)
def get_fundamentalInSector(symbol:str):
   df = getFundamentalInSector(symbol,progress=False) 
   #print(df.columns)
   df['ROE/PBV'] = df['ROE(%)']/df['P/BV (เท่า)']
   df = df.sort_values('ROE/PBV',ascending=False)  
   return getCss()+df.to_html(index=False) # 

#######################################################################################################################
@app.get("/stockprice/{symbol}", response_class=HTMLResponse)
async def get_histPrice(symbol:str):
    tv = TvDatafeed()
    df = tv.get_hist(symbol=symbol,exchange='set',interval=Interval.in_daily,n_bars=100)
    df['Date'] = pd.to_datetime(df.index)
    df['Date'] = df['Date'].dt.strftime('%Y-%m-%d')
    df['Date'] = pd.to_datetime(df['Date'])
    df['no'] = np.array(range(1,len(df)+1))
    
    df = df.reset_index()
    df = df[['no','Date','open','high','low','close','volume']]  
    
    return getCss()+df.to_html(index=False) #