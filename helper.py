from ast import Await
import requests
from bs4 import BeautifulSoup
import pandas as pd 
import numpy as np
from datetime import datetime as dt

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

from tvDatafeed import TvDatafeed, Interval


from datetime import datetime,timedelta
import pytz

def findRangeDateRS():
    
   beginDate = '2022-01-04'  #วันแรกที่มีการเทรดของปีใหม่

   tz = pytz.timezone('Asia/Bangkok')
   ct = datetime.now(tz=tz)
   ct.strftime('%Y-%m-%d')
    
   tz = pytz.timezone('Asia/Bangkok')
   c1 = datetime.now(tz=tz)
   d1str = c1.strftime('%Y-%m-%d %H:%M:%S')

   d2str = c1.strftime('%Y-%m-%d')
   d2str = d2str + ' 18:00:00'

   d1 = datetime.strptime(d1str, '%Y-%m-%d %H:%M:%S')  #datenow
   d2 = datetime.strptime(d2str, '%Y-%m-%d %H:%M:%S')  #datenow only day at 18.00
   
   if((d1-d2) > timedelta(minutes=1)):  
      endDate = c1.strftime('%Y-%m-%d')
        
   else:
     yesterday =  d1 - timedelta(days=1)
     yesterdaystr = yesterday.strftime('%Y-%m-%d') 
     endDate  = yesterdaystr
        
        
   return beginDate,endDate


######################################


def getRSRanking(beginDate,endDate):
    try:
       RS = pd.read_csv(endDate+'.csv')
    except:
       
       #ดึงข้อมูลสมาชิกใน SET100
       basket = getMemberOfIndex('set100')

       #ใช้มาตรฐานตัววัดคือ SET Index
       SET = pd.DataFrame({'symbol':'SET'},index=[0])

       #basket = basket.append(SET,ignore_index=True)
       basket = pd.concat([basket,SET],ignore_index=True)

       #ดึงราคาของหุ้นใน set100 และ setindex มา
       obj = HistStockPrice() #สร้างตัวดึงราคาจาก 
       p=0
       for i in basket.symbol:
        if(p == 0):
          df = obj.getPrice(i,start=beginDate)
          p=1
        else:
          k = obj.getPrice(i,start=beginDate)
          #df[i] = k
          df = pd.concat([df,k], axis=1)  

        if(i!='SET'):
          print(i,end=',') 
        else:
          print(i,end='')

       print(df)
       df = np.round(df,2)  #ปัดเศษ
       
       endDateRR = df.index[-1].strftime('%Y-%m-%d') #ป้องกันการโหลดข้อมูลที่ไม่เจอ
       
       SETBegin = df[df.index==beginDate]['SET'][0]
       SETEnd = df[df.index==endDateRR]['SET'][0]    

       RS = (df[df.index==endDateRR].values / df[df.index==beginDate].values) / (SETEnd/SETBegin)
       RS = pd.DataFrame({'RS':RS[0]},index=df.columns)
       RS = rankWithRange(data=RS,minScope=1,maxScope=99)
        
       RS = RS.reset_index()
       RS['no'] = range(1,101+1)
       RS = RS[['no','index','RS','RANK','RS_Rank']]
       RS.columns = ['no','symbol','RS','RANK','RS Rank']
   
       RS.to_csv(endDate+'.csv')
        
    return RS

######################################

def rankWithRange(data,minScope,maxScope):
    basket = data.sort_values([data.columns[0]],ascending=False)

    basket['RANK'] = list(range(len(basket), 0,-1))
    maxRank = basket['RANK'].max()
    minRank = basket['RANK'].min()
    maxScope = maxScope
    minScope = minScope
    S = (minScope-maxScope)/(minRank-maxRank)
    Int = minScope - S*minRank
    basket['RS_Rank'] = basket['RANK']*S+Int  
    
    return basket

class HistStockPrice():
  def __init__(self):    
     self.tv = TvDatafeed()
    
  def days_between(self,d1, d2):
      d1 = dt.strptime(d1, "%Y-%m-%d")
      d2 = dt.strptime(d2, "%Y-%m-%d")
      return abs((d2 - d1).days)

  def getPrice(self,symbol,start,stop='',exchange='set'):
    
      date_now = dt.today().strftime("%Y-%m-%d") 
    
      if(stop==''):
        stop = date_now
    
      k = self.days_between(start, date_now)
    
      df = self.tv.get_hist(symbol=symbol,exchange=exchange,interval=Interval.in_daily,n_bars=k)
    
      df['Date'] = pd.to_datetime(df.index)
      df['Date'] = df['Date'].dt.strftime('%Y-%m-%d')
      df['Date'] = pd.to_datetime(df['Date'])

      df = df[['Date','close']]
      df = df.set_index('Date')
      df.columns = [symbol]
    
      df = df[(df.index>=start) & (df.index<=stop)]
      return df





def getMemberOfIndex(indexType):
 '''
    arg1 (str): ตัวอย่างเช่น SET50,SET100,sSET,SETCLMV,SETHD,SETTHSI,SETWB 
           
    Returns (dataFrame) '''
    
 symbol = indexType.upper()
 if(symbol=='SSET'):
    symbol = 'sSET'

 if(symbol=="SET"):
   print("SET NOT Support ,หรืออาจใช้ sx.listSecurities เรียกใช้แทน")
   return 0

 headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"}   
                  
 r = requests.post("https://marketdata.set.or.th/mkt/sectorquotation.do?sector="+symbol+"&language=th&country=TH",verify=False,headers=headers)
 r.content
 soup = BeautifulSoup(r.content, "lxml")  
 dtmpCheck = soup.find_all("table", {"class" : "table-info"})


 ################# ปรับสำหรับ mai เพราะจะมีหลายกลุ่มอุตสาหกรรม #####################################
 if(symbol=="mai"):
  datasource = []
  for ttable in range(2,len(dtmpCheck)):
    g_data = dtmpCheck[ttable].find_all("td",{"style":"text-align: left;"})
    for i in g_data:
      datasource.append(i.text.strip()) 
 
  df = pd.DataFrame(datasource)
  df.columns = ["symbol"]
  return df
 ################# End Case mai  #########################


 if(len(dtmpCheck)==0):
   print("ไม่พบข้อมูล")
   return 0
 g_data = dtmpCheck[2].find_all("td",{"style":"text-align: left;"})
  
 datasource = []
 for i in g_data:
  datasource.append(i.text.strip())   
 
 df = pd.DataFrame(datasource)
 df.columns = ["symbol"]
 return df

def trimdata(s):
  s = s.replace(',', '')
  s = s.replace('*', '')
  #s = s.replace('-', '0')
  s = s.rstrip()
  s = s.lstrip() 
  return s
  
######################################
def getdata(symbol,returntype="ar"):
 symboltmp = symbol.replace('&', '+%26+') 
 datasource = []   
 headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36;Content-Type: text/html; charset=UTF-8"} 
 #url = "http://www.settrade.com/C04_06_stock_financial_p1.jsp?txtSymbol="+symboltmp  

 #แก้ไขใน version 2021-10-เมษายน เพราะ url เดิมไม่ได้คืนค่ามาให้ version 0.155530
 url = 'https://www.settrade.com/sims/dat/finstmt/files/'+symboltmp+'_th_TH.html'

 #แก้ไขใน version 2021-10-เมษายน เปลี่ยนเป็น get version 0.155530
 r = requests.get(url,headers)
 #r.content
 r.encoding = 'ISO-8859-1'
 k = r.content#.encode('utf-8', errors="replace")
 
 soup = BeautifulSoup(k,'lxml',from_encoding='UTF-8')  
 
 try:
  g_data = soup.find_all("table", {"class" : "table table-hover table-info"})[0].find_all("tr")
 except:
  #case S&J
  return 0
 #################    
 #print('*****getata*******')
 #print(g_data)
 col = []
 datasource.append(symbol)
  
 for k in g_data:
    td = k.find_all("td")
    for m in td:
       tmp = m.text.strip()
  
       if(tmp!=""):
        tmp = tmp.replace(',', '').encode('utf-8').decode("utf-8")
        print('E7802-231----- ',tmp)
        
        
        if(tmp=="สินทรัพย์รวม" or tmp=='ÊÔ¹·ÃÑ¾ÂìÃÇÁ'):
           col.append(tmp)
           lenindex = len(td)
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp)
         
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
           
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan

           if(tmp=="" or lenindex<=2): #เป็นพวกกองทุนรวม
             return 0

           datasource.append(float(tmp))   
              

           break

        if(tmp=="หนี้สินรวม" or tmp=='Ë¹ÕéÊÔ¹ÃÇÁ'):
           col.append(tmp)
           lenindex = len(td)
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp)
           
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
           
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan

           if(tmp=="" or lenindex<=2): #เป็นพวกกองทุนรวม
             return 0
           datasource.append(float(tmp))   
           break

        if(tmp=="ส่วนของผู้ถือหุ้น" or tmp=='ÊèÇ¹¢Í§¼Ùé¶×ÍËØé¹'):
           col.append(tmp)
           lenindex = len(td)
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp)
            
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
           
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan

           if(tmp=="" or lenindex<=2): #เป็นพวกกองทุนรวม
             return 0
           datasource.append(float(tmp))   
           break
            
        if(tmp=="รายได้รวม" or tmp=='ÃÒÂä´éÃÇÁ'):
           col.append(tmp)
           lenindex = len(td)
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp)
            
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
           
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan

           if(tmp==""): #เป็นพวกกองทุนรวม
             return 0
            
           datasource.append(float(tmp))   
           break

        if(tmp=="กำไรสุทธิ" or tmp=='¡ÓäÃÊØ·¸Ô'):
           col.append(tmp)
           lenindex = len(td)
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp)
           
          
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)

           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan
              
           if(tmp==""): #เป็นพวกกองทุนรวม
             return 0
           datasource.append(float(tmp))   
           break
            
        if(tmp=="ROE(%)"):
           col.append(tmp)
           lenindex = len(td)
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp)
            
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
                   
           if(tmp=='N/A' or tmp=='N.A.'):
              tmp = np.nan  
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan

           if(tmp==""): #เป็นพวกกองทุนรวม
             return 0     
           datasource.append(float(tmp))   
           break
            
        if(tmp=="มูลค่าหลักทรัพย์ตามราคาตลาด" or 'ÁÙÅ¤èÒËÅÑ¡·ÃÑ¾ÂìµÒÁÃÒ¤Òµ' in tmp):
           col.append(tmp)
           lenindex = len(k.find_all("td"))
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp) 
            
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
          
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan
           datasource.append(float(tmp))   
           break
            
        if(tmp=="P/E (เท่า)" or tmp=='P/E (à·èÒ)'):
           col.append(tmp)
           lenindex = len(k.find_all("td"))
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp) 
            
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
            
           if(tmp=='N/A' or tmp=='N.A.'):
              tmp = np.nan 
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan

           datasource.append(float(tmp))   
           break
            
        if(tmp=="P/BV (เท่า)" or tmp=='P/BV (à·èÒ)'):
           col.append(tmp)
           lenindex = len(k.find_all("td"))
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp) 
            
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
            
           if(tmp=='N/A'):
              tmp = 0  
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan   
           datasource.append(float(tmp))   
           break    
        
        if(tmp=="อัตราส่วนเงินปันผลตอบแทน(%)" or tmp=='ÍÑµÃÒÊèÇ¹à§Ô¹»Ñ¹¼ÅµÍºá·¹(%)'):
           col.append(tmp)
           lenindex = len(k.find_all("td"))
           tmp = k.find_all("td")[lenindex-1].text.strip()
           tmp = trimdata(tmp) 
            
           if(tmp==""):
             tmp = k.find_all("td")[lenindex-2].text.strip()
             tmp = trimdata(tmp)
            
           if(tmp=='N/A' or tmp=='N.A.'):
              tmp = np.nan  
           if(tmp=="-"): #fix bug 0.15541 กรณี ข้อมูลส่งมาเป็น "-"
             tmp = np.nan    
           datasource.append(float(tmp))   
           break
 
 
 if returntype=="df":
     datasource = pd.DataFrame(datasource).T
     datasource.columns = ["symbol","สินทรัพย์รวม","หนี้สินรวม","ส่วนของผู้ถือหุ้น","รายได้รวม","กำไรสุทธิ","ROE(%)","มูลค่าหลักทรัพย์ตามราคาตลาด","P/E (เท่า)","P/BV (เท่า)","อัตราส่วนเงินปันผลตอบแทน(%)"]  
         
 return datasource 


def listStockInSector(sample_in_sector):
 sample_in_sector = sample_in_sector.replace("&","%26")  
 headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"}   
 r = requests.post("http://www.settrade.com/C04_08_stock_sectorcomparison_p1.jsp?txtSymbol="+sample_in_sector,headers=headers)
 #r.content
 soup = BeautifulSoup(r.content, "lxml")  
 g_data = soup.find_all("table", {"class" : "table table-info"})
 
 if(len(g_data)==0):
   print("ไม่พบข้อมูล")  
   return 0
 
 g_data = g_data[0].find_all("td",{"class" : "text-left"})
 
 listname_Sector = []
 for k in g_data:
   listname_Sector.append(k.text.strip())
 
 p = pd.DataFrame(listname_Sector)   
 p.columns = ["symbol"]   
 return p

#แก้ใน version 0.1544
def getFundamentalInSector(sample_in_sector,progress=True):
 datasource = []
 p = listStockInSector(sample_in_sector=sample_in_sector) 
 print(p)      
 for k in range(len(p)):   
    r = p.loc[k].values
    s = getdata(str(r[0]),returntype='ar')
    if(s!=0):
     if(progress==True): 
       print(r, end='')
       pass   
     datasource.append(s) 

 if(progress==True): 
   print("..Complete", end='')
   pass
 datasource = pd.DataFrame(datasource)

 print('*VIV-K0324**')
 print(datasource)
 print('***')
 datasource.columns = ["symbol","สินทรัพย์รวม","หนี้สินรวม","ส่วนของผู้ถือหุ้น","รายได้รวม","กำไรสุทธิ","ROE(%)","มูลค่าหลักทรัพย์ตามราคาตลาด","P/E (เท่า)","P/BV (เท่า)","อัตราส่วนเงินปันผลตอบแทน(%)"]   
 return datasource  


def getCss():
    #https://medium.com/analytics-vidhya/5-css-snippets-for-better-looking-pandas-dataframes-ad808e407894
    stylecss = '''<style>.dataframe th{
        background: rgb(61,164,166);
        background: linear-gradient(0deg, rgba(61,164,166,1) 0%, rgba(31,198,190,1) 100%);
        padding: 10px;
        font-family: arial;
        font-size: 120%;
        color: white;
        border:2px solid white;
        text-align:left !important;}
        </style>'''
    return stylecss